//
// Created by Olsal on 16.12.2020.
//

#ifndef ZOOPROJECT_INVENTORY_H
#define ZOOPROJECT_INVENTORY_H

#include <iostream>
#include "Location.h"


class Inventory {
    Location* m_weapon;
    Location* m_armor;
public:
    Inventory();
    void pickUpItem(Location* item);
    void dropWeapon();
    void dropArmor();
    int getDeffenseBonus();
    int getAttackBonus();
    void printInventoryInfo();
};


#endif //ZOOPROJECT_INVENTORY_H
