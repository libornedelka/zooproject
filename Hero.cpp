//
// Created by Olsal on 16.12.2020.
//

#include "Hero.h"
Hero::Hero(std::string name){
    m_health = 100;
    m_attack = 10;
    m_armor = 5;
    m_name = name;
}

int Hero::getAttack(){
    return m_attack;
}

int Hero::getHealth(){
    return m_health;
}

/* upravit v Inventory aby fungovalo
void Hero::setArmor(Inventory* defenseBonus){
    m_armor += defenseBonus->getDeffenseBonus();
}

void Hero::setAttack(Inventory* attackBonus){
    m_attack += attackBonus->getAttackBonus();
}
 */

int Hero::getArmor(){
    return m_armor;
}

void Hero::printInfo(){
    std::cout << "Hero's name: " << m_name << std::endl;
    std::cout << "Hero's health: " << m_health << std::endl;
    std::cout << "Hero's armor: " << m_armor << std::endl;
    std::cout << "Hero's attack: " << m_attack << std::endl;
    //std::cout << "Hero's inventory: " << Inventory().printInventoryInfo() << std::endl;
}